package net.octopus.alium.demo.ocrrecharge;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IdRes;

import it.sephiroth.android.library.bottomnavigation.BottomNavigation;

/**
 * Created by Abdul-Mujeeb Aliu on 6/11/2016.
 */
public class BottomConfigurator implements BottomNavigation.OnMenuItemSelectionListener {
    private BottomNavigation mBottomNavigation;
    private Context mContext;

    public BottomConfigurator(BottomNavigation mBottomNavigation, Context mContext) {
        this.mBottomNavigation = mBottomNavigation;
        this.mContext = mContext;
    }

    @Override
    public void onMenuItemSelect(@IdRes int i, int i1) {
        if (i == R.id.bbn_item1) {
            Intent intent = new Intent(mContext, CaptureActivity.class);
            mBottomNavigation.getContext().startActivity(intent);
        } else if (i == R.id.bbn_item2) {
            Intent intent = new Intent(mContext, QRActivity.class);
            mBottomNavigation.getContext().startActivity(intent);
        } else {
            Intent intent = new Intent(mContext, PreferencesActivity.class);
            mBottomNavigation.getContext().startActivity(intent);
        }
    }

    @Override
    public void onMenuItemReselect(@IdRes int i, int i1) {
        if (i == R.id.bbn_item1) {
            Intent intent = new Intent(mContext, CaptureActivity.class);
            mBottomNavigation.getContext().startActivity(intent);
        } else if (i == R.id.bbn_item2) {
            Intent intent = new Intent(mContext, QRActivity.class);
            mBottomNavigation.getContext().startActivity(intent);
        } else {
            Intent intent = new Intent(mContext, PreferencesActivity.class);
            mBottomNavigation.getContext().startActivity(intent);
        }
    }
}
